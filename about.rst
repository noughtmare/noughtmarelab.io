---
title: About
---

My name is Jaro Reinders. I am currently researching modular dynamic semantics specification as a PhD in the `Programming Languages group <https://pl.ewi.tudelft.nl/>`_ at Delft University of Technology. In my free time I like to write Haskell programs (like this one).
