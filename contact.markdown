---
title: Contact
---

The preferred way of contacting me is to mail me at
[jaro.reinders@gmail.com](mailto:jaro.reinders@gmail.com). You can also try to catch me
on IRC -- I lurk as `Naughtmare[m]` in the `#haskell` channel on [freenode]. My matrix ID is `@Naughtmare:matrix.org`.

[freenode]: http://freenode.net/
