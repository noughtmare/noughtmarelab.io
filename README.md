![Build Status](https://gitlab.com/pages/hakyll/badges/master/build.svg)

---

My [Hakyll] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: haskell:7.10.3

pages:
  cache:
    paths:
      - _cache
      - .stack
  before_script:
    - export STACK_ROOT=`pwd`/.stack
    - stack install --only-dependencies
    - stack build
  script:
    - stack exec site build
  artifacts:
    paths:
      - public
  only:
    - master
```

Initial build may take some time (around 30 minutes), following builds will be significantly faster.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hakyll.
1. Generate the website: `stack exec site build`
1. Preview your project: `stack exec site watch`
1. Add content

Read more at Hakyll's [documentation][hakyll].

## Getting help
* [Tutorials](https://jaspervdj.be/hakyll/tutorials.html)
* [Google discussion group](https://groups.google.com/forum/#!forum/hakyll)
* [Hakyll on StackOverflow](https://stackoverflow.com/questions/tagged/hakyll)

---

Forked from https://gitlab.com/jtojnar/hakyll

[ci]: https://about.gitlab.com/gitlab-ci/
[hakyll]: https://jaspervdj.be/hakyll/
[install]: https://jaspervdj.be/hakyll/tutorials/01-installation.html
[documentation]: http://link-to-main-documentation-page
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
